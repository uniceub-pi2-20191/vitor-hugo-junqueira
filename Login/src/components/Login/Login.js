import React, {Component} from 'react';
import {View, Image, StyleSheet} from 'react-native';
import LoginForm from './LoginForm';

export default class Login extends Component {
	render() {
		return (
			<View style={styles.container}>

				<View style={styles.inner}>
					<Image style={styles.image} source={require('../images/logo.jpeg')}/>
				</View>

                <View style={styles.form}>
					<LoginForm/>
				</View>

			</View>	
			);}
}


const styles = StyleSheet.create({
  container: {
  	backgroundColor: '#2FB320',
  	flex: 1,
  	//flex define o tamanho do elemento em relação aos demais elementos dentro do container
  },

  form: {
  	//alinhamento do conteudo no primeiro eixo (dependendo do eixo definido anteriormente) 
  	//ex: caso o flexDirection seja column, o justifyContent representara o eixo horizontal
  	justifyContent: 'center', 

  	//alinhamento do conteudo nao segundo eixo
	alignItems: 'center',

	//flexgrow determina o quanto um componente vai crescer em relação aos demais componentes de  um memso container
	flexGrow: 1 
  },

  inner: {
  	//determina se a flex inicia de cima para baixo (column) ou da esquerda para a direita(row)
    flexDirection: 'column',
	justifyContent: 'center', 
	alignItems: 'center',
	flexGrow: 1, 
  },

  image: {
  	width:  100, 
  	height: 100,
  },

});
