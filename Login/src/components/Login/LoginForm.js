import React, {Component} from 'react';
import {View, TextInput, StyleSheet, TouchableOpacity, Text, Alert} from 'react-native';

class LoginForm extends Component {

	constructor(props) {
		// props são valores fixos de um componente
	    super(props);
	    // states são valores variáveis
	    this.state = { email: '', password: ''};
	}

	setEmail(email) {
		this.state.email = email
	}

	setPassword(password) {
		this.state.password = password
	}  


	onPressButton() {
		//Alert.alert("Efetuando Login")
		Alert.alert(this.state.password)	
	}

	render() {
		return (
			<View style={styles.container}>
				<TextInput style={styles.input}
						   onChangeText={(text) => this.setEmail(text)}
						   autoCapitalize='none'
						   autoCorrect={false}
						   keyboardType='email-address'
						   placeholder='Email'
						   placeholderColor='rgba(255,255,255,0.7)'
				/>
				<TextInput style={styles.input}
					     onChangeText={(text) => this.setPassword(text)}
						   placeholder='Password'
						   placeholderColor='rgba(255,255,255,0.7)'
						   secureTextEntry/>
				<TouchableOpacity style={styles.button} onPress={() => this.onPressButton()}>
					<Text style={styles.textButton}> Login </Text>
				</TouchableOpacity>
			</View>

				//Por que utilizar TouchableOpacity no lugar de button ?
				//Por que o height do button não muda no app quando é alterado no código?
				//qual é a funcionalidade do padding, visto que, a principio, ao elimina-lo do código
				//nada muda?
		);
	}
}

const styles = StyleSheet.create({

	container: {
		padding: 20
	},

	input: {

		height: 60,
		width: 300,
		backgroundColor: 'rgba(255,255,255,0.2)',
		padding: 10,
		color: '#fff'

	},

	button: {
		height: 50,
		width: 300,
		padding: 15,
		backgroundColor: '#ff6505'
	},

	textButton: {
		color: '#fff',
		textAlign: 'center',
		fontWeight: '100',
		fontSize: 40
	}

});


export default LoginForm;
